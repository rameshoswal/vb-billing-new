﻿Imports System.Data.OleDb
Public Class BILLINGFORM
    Dim bill_no As Integer
    Dim cmd As OleDb.OleDbCommand
    Dim dt, dtnew, dtbillno, dtadd As New DataTable
    Dim da As OleDbDataAdapter
    Dim mystr As String
    Private Sub BILLINGFORM_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       

        mystr = "Select item_name from item"
        itemadd.connection.Open()
        cmd = New OleDbCommand(mystr, itemadd.connection)
        da = New OleDbDataAdapter(cmd)
        dt.Clear()
        da.Fill(dt)
        itemcombobox.DataSource = dt
        itemcombobox.DisplayMember = "item_name"
        itemcombobox.ValueMember = "item_name"


        'get highest old bill no. 
        mystr = "select max(bill_id) from bill"
        cmd = New OleDbCommand(mystr, itemadd.connection)
        
        Dim temp As Integer = cmd.ExecuteScalar()
        bill_no = temp + 1
        billnolabel.Text = bill_no
        mystr = "insert into bill values(" & bill_no & ",0)"
        cmd = New OleDbCommand(mystr, itemadd.connection)
        cmd.ExecuteNonQuery()
    End Sub

    Private Sub itemcombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles itemcombobox.SelectedIndexChanged
    

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        mystr = "select price,item_id from item where item_name = '" & itemcombobox.Text & "'"
        cmd = New OleDbCommand(mystr, itemadd.connection)
        Dim dr As OleDbDataReader = cmd.ExecuteReader()
        dr.Read()

        Label4.Text = dr.GetInt32(0)
        Label5.Text = dr.GetInt32(1)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim total As Integer
        total = Integer.Parse(qtytb.Text) * Integer.Parse(Label4.Text)
        mystr = "insert into sale values(" & bill_no & "," & Label5.Text & "," & qtytb.Text & "," & total & ")"
        cmd = New OleDbCommand(mystr, itemadd.connection)
        cmd.ExecuteNonQuery()

        mystr = "select * from sale where bill_no = " & bill_no
        cmd = New OleDbCommand(mystr, itemadd.connection)
        da = New OleDbDataAdapter(cmd)
        dtnew = New DataTable
        da.Fill(dtnew)

        billdatagrid.DataSource = dtnew

        totallabel.Text = Integer.Parse(totallabel.Text) + total

        qtytb.Text = ""
    End Sub

    Private Sub Label7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label7.Click

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        mystr = "update bill set total = " & totallabel.Text & " where bill_id = " & bill_no
        cmd = New OleDbCommand(mystr, itemadd.connection)
        cmd.ExecuteNonQuery()

        finalbill.Show()
        Me.Hide()
    End Sub
End Class