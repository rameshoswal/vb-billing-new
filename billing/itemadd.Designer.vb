﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class itemadd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.add_item_bt = New System.Windows.Forms.Button()
        Me.itemname_tb = New System.Windows.Forms.TextBox()
        Me.itemprice_tb = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Item_datagrid = New System.Windows.Forms.DataGridView()
        Me.ItemidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ItemNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PriceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ItemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BillingDataSet = New billing.billingDataSet()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ItemTableAdapter = New billing.billingDataSetTableAdapters.itemTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.Item_datagrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ItemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BillingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'add_item_bt
        '
        Me.add_item_bt.Location = New System.Drawing.Point(92, 206)
        Me.add_item_bt.Name = "add_item_bt"
        Me.add_item_bt.Size = New System.Drawing.Size(75, 23)
        Me.add_item_bt.TabIndex = 0
        Me.add_item_bt.Text = "add new"
        Me.add_item_bt.UseVisualStyleBackColor = True
        '
        'itemname_tb
        '
        Me.itemname_tb.Location = New System.Drawing.Point(92, 78)
        Me.itemname_tb.Name = "itemname_tb"
        Me.itemname_tb.Size = New System.Drawing.Size(100, 20)
        Me.itemname_tb.TabIndex = 1
        '
        'itemprice_tb
        '
        Me.itemprice_tb.Location = New System.Drawing.Point(92, 132)
        Me.itemprice_tb.Name = "itemprice_tb"
        Me.itemprice_tb.Size = New System.Drawing.Size(100, 20)
        Me.itemprice_tb.TabIndex = 2
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(186, 206)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "update"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Item_datagrid
        '
        Me.Item_datagrid.AutoGenerateColumns = False
        Me.Item_datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Item_datagrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ItemidDataGridViewTextBoxColumn, Me.ItemNameDataGridViewTextBoxColumn, Me.PriceDataGridViewTextBoxColumn})
        Me.Item_datagrid.DataSource = Me.ItemBindingSource
        Me.Item_datagrid.Location = New System.Drawing.Point(317, 79)
        Me.Item_datagrid.Name = "Item_datagrid"
        Me.Item_datagrid.Size = New System.Drawing.Size(359, 150)
        Me.Item_datagrid.TabIndex = 4
        '
        'ItemidDataGridViewTextBoxColumn
        '
        Me.ItemidDataGridViewTextBoxColumn.DataPropertyName = "item_id"
        Me.ItemidDataGridViewTextBoxColumn.HeaderText = "item_id"
        Me.ItemidDataGridViewTextBoxColumn.Name = "ItemidDataGridViewTextBoxColumn"
        '
        'ItemNameDataGridViewTextBoxColumn
        '
        Me.ItemNameDataGridViewTextBoxColumn.DataPropertyName = "item name"
        Me.ItemNameDataGridViewTextBoxColumn.HeaderText = "item name"
        Me.ItemNameDataGridViewTextBoxColumn.Name = "ItemNameDataGridViewTextBoxColumn"
        '
        'PriceDataGridViewTextBoxColumn
        '
        Me.PriceDataGridViewTextBoxColumn.DataPropertyName = "price"
        Me.PriceDataGridViewTextBoxColumn.HeaderText = "price"
        Me.PriceDataGridViewTextBoxColumn.Name = "PriceDataGridViewTextBoxColumn"
        '
        'ItemBindingSource
        '
        Me.ItemBindingSource.DataMember = "item"
        Me.ItemBindingSource.DataSource = Me.BillingDataSet
        '
        'BillingDataSet
        '
        Me.BillingDataSet.DataSetName = "billingDataSet"
        Me.BillingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "item name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 135)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "price"
        '
        'ItemTableAdapter
        '
        Me.ItemTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(396, 328)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(212, 70)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Next form"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'itemadd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(701, 425)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Item_datagrid)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.itemprice_tb)
        Me.Controls.Add(Me.itemname_tb)
        Me.Controls.Add(Me.add_item_bt)
        Me.Name = "itemadd"
        Me.Text = "Form1"
        CType(Me.Item_datagrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ItemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BillingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents add_item_bt As System.Windows.Forms.Button
    Friend WithEvents itemname_tb As System.Windows.Forms.TextBox
    Friend WithEvents itemprice_tb As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Item_datagrid As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BillingDataSet As billing.billingDataSet
    Friend WithEvents ItemBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ItemTableAdapter As billing.billingDataSetTableAdapters.itemTableAdapter
    Friend WithEvents ItemidDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ItemNameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PriceDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class
